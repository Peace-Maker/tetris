# Tetris

Play Tetris on your watch!

Right two buttons move the piece horizontally, bottom left button rotates it clock-wise.